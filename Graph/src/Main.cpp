#include "Graph.h"

int main() {
    Graph g;
    g.readGraph("res/flow.txt");
    g.transformToListOfEdges();
    g.flowFordFulkerson(1, 6).writeGraph("out/flow_ford.txt");
    g.transformToListOfEdges();
    g.flowDinitz(1, 6).writeGraph("out/flow_dinitz.txt");
    return 0;
}
