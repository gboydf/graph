#ifndef GRAPH_H
#define GRAPH_H

#include <algorithm>
#include <fstream>
#include <iostream>
#include <istream>
#include <limits>
#include <map>
#include <ostream>
#include <queue>
#include <sstream>
#include <stack>
#include <string>
#include <utility>
#include <vector>


////////////////////////////////////////////////////////////////////////////////////////////////////
// Disjoint set union.
////////////////////////////////////////////////////////////////////////////////////////////////////

class DSU {
private:
    std::vector<int> p;
    std::vector<int> r;

public:
    DSU(int n)
        : p(n + 1), r(n + 1)
    {
        for (int i = 0; i <= n; ++i) {
            make(i);
        }
    }

    void make(int v) {
        p[v] = v;
        r[v] = 0;
    }

    int find(int v) {
        if (v == p[v]) {
            return v;
        }
        return p[v] = find(p[v]);
    }

    void join(int a, int b) {
        a = find(a);
        b = find(b);
        if (a != b) {
            if (r[a] < r[b]) {
                p[a] = b;
            }
            else {
                p[b] = a;
            }
            if (r[a] == r[b]) {
                ++r[a];
            }
        }
    }
};


////////////////////////////////////////////////////////////////////////////////////////////////////
// Interface section.
////////////////////////////////////////////////////////////////////////////////////////////////////

class IGraph {
public:
    virtual ~IGraph() = default;

    virtual void readGraph (std::istream& stream) = 0;
    virtual void writeGraph(std::ostream& stream) = 0;

    virtual void addEdge   (int from, int to, int weight) = 0;
    virtual void removeEdge(int from, int to) = 0;
    virtual int  changeEdge(int from, int to, int weight) = 0;
    virtual int  getEdge   (int from, int to) = 0;

    virtual IGraph* transformToAdjMatrix  () = 0;
    virtual IGraph* transformToAdjList    () = 0;
    virtual IGraph* transformToListOfEdges() = 0;

    virtual std::vector<std::vector<int>>& getAdjMatrix() {
        throw 0;
    }

    virtual std::vector<std::map<int, int>>& getAdjList() {
        throw 0;
    }

    virtual std::vector<std::tuple<int, int, int>>& getListOfEdges() {
        throw 0;
    }

    virtual IGraph* getCopy() = 0;
    virtual void    getProperties(int& n, int& r, int& w) = 0;
    virtual int     getSize() = 0;
};


////////////////////////////////////////////////////////////////////////////////////////////////////
// AdjMatrix graph representation.
////////////////////////////////////////////////////////////////////////////////////////////////////

class AdjMatrixGraph : public IGraph {
private:
    std::vector<std::vector<int>> data;
    int n;
    int r;
    int w;

public:
    AdjMatrixGraph(int n = 0, int r = 0, int w = 0)
        : data(n, std::vector<int>(n, 0)), n(n), r(r), w(w)
    {
    }

    void readGraph (std::istream& stream) override;
    void writeGraph(std::ostream& stream) override;

    void addEdge   (int from, int to, int weight) override;
    void removeEdge(int from, int to) override;
    int  changeEdge(int from, int to, int weight) override;
    int  getEdge   (int from, int to) override;

    IGraph* transformToAdjMatrix  () override;
    IGraph* transformToAdjList    () override;
    IGraph* transformToListOfEdges() override;

    std::vector<std::vector<int>>& getAdjMatrix() override;

    IGraph* getCopy() override;
    void    getProperties(int& n, int& r, int& w) override;
    int     getSize() override;
};


////////////////////////////////////////////////////////////////////////////////////////////////////
// AdjList graph representation.
////////////////////////////////////////////////////////////////////////////////////////////////////

class AdjListGraph : public IGraph {
private:
    std::vector<std::map<int, int>> data;
    int n;
    int r;
    int w;

public:
    AdjListGraph(int n = 0, int r = 0, int w = 0)
        : data(n), n(n), r(r), w(w)
    {
    }

    void readGraph (std::istream& stream) override;
    void writeGraph(std::ostream& stream) override;

    void addEdge   (int from, int to, int weight) override;
    void removeEdge(int from, int to) override;
    int  changeEdge(int from, int to, int weight) override;
    int  getEdge   (int from, int to) override;

    IGraph* transformToAdjMatrix  () override;
    IGraph* transformToAdjList    () override;
    IGraph* transformToListOfEdges() override;

    std::vector<std::map<int, int>>& getAdjList() override;

    IGraph* getCopy() override;
    void    getProperties(int& n, int& r, int& w) override;
    int     getSize() override;
};


////////////////////////////////////////////////////////////////////////////////////////////////////
// ListOfEdges graph representation.
////////////////////////////////////////////////////////////////////////////////////////////////////

class ListOfEdgesGraph : public IGraph {
public:
    struct EdgeComparator {
        bool operator()(const std::tuple<int, int, int>& a, const std::tuple<int, int, int>& b) {
            int aa;
            int ba;
            int wa;
            int ab;
            int bb;
            int wb;
            std::tie(aa, ba, wa) = a;
            std::tie(ab, bb, wb) = b;
            if (wa < wb) {
                return true;
            }
            else if (wa == wb) {
                if (aa < ab) {
                    return true;
                }
                else if (aa == ab) {
                    if (ba < bb) {
                        return true;
                    }
                }
            }
            return false;
        }
    };

private:
    std::vector<std::tuple<int, int, int>> data;
    int n;
    int m;
    int r;
    int w;

public:
    ListOfEdgesGraph(int n = 0, int r = 0, int w = 0)
        : n(n), m(0), r(r), w(w)
    {
    }

    void readGraph (std::istream& stream) override;
    void writeGraph(std::ostream& stream) override;

    void addEdge   (int from, int to, int weight) override;
    void removeEdge(int from, int to) override;
    int  changeEdge(int from, int to, int weight) override;
    int  getEdge   (int from, int to) override;

    IGraph* transformToAdjMatrix  () override;
    IGraph* transformToAdjList    () override;
    IGraph* transformToListOfEdges() override;

    std::vector<std::tuple<int, int, int>>& getListOfEdges() override;

    IGraph* getCopy() override;
    void    getProperties(int& n, int& r, int& w) override;
    int     getSize() override;
};


////////////////////////////////////////////////////////////////////////////////////////////////////
// Implementation section.
////////////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////////////////
// AdjMatrix representation.
////////////////////////////////////////////////////////////////////////////////////////////////////

void AdjMatrixGraph::readGraph(std::istream& stream) {
    stream >> n;
    stream >> r >> w;
    std::vector<std::vector<int>>(n, std::vector<int>(n, 0)).swap(data);
    for (int i = 0; i < n; ++i) {
        for (int j = 0; j < n; ++j) {
            stream >> data[i][j];
        }
    }
}

void AdjMatrixGraph::writeGraph(std::ostream& stream) {
    stream << "C" << " " << n << "\n";
    stream <<  r  << " " << w << "\n";
    for (int i = 0; i < n; ++i) {
        for (int j = 0; j < n; ++j) {
            stream << data[i][j] << ((j < n - 1) ? " " : "\n");
        }
    }
}

void AdjMatrixGraph::addEdge(int from, int to, int weight) {
    data[from - 1][to - 1] = weight;
    if (r == 0) {
        data[to - 1][from - 1] = weight;
    }
}

void AdjMatrixGraph::removeEdge(int from, int to) {
    data[from - 1][to - 1] = 0;
    if (r == 0) {
        data[to - 1][from - 1] = 0;
    }
}

int AdjMatrixGraph::changeEdge(int from, int to, int weight) {
    auto temp = data[from - 1][to - 1];
    data[from - 1][to - 1] = weight;
    if (r == 0) {
        data[to - 1][from - 1] = weight;
    }
    return temp;
}

int AdjMatrixGraph::getEdge(int from, int to) {
    return (data[from - 1][to - 1] != 0) ? data[from - 1][to - 1] : -1;
}

IGraph* AdjMatrixGraph::transformToAdjMatrix() {
    return this;
}

IGraph* AdjMatrixGraph::transformToAdjList() {
    auto temp = new AdjListGraph(n, r, w);
    for (int i = 0; i < n; ++i) {
        for (int j = ((r == 1) ? 0 : i); j < n; ++j) {
            if (data[i][j] != 0) {
                temp->addEdge(i + 1, j + 1, data[i][j]);
            }
        }
    }
    return temp;
}

IGraph* AdjMatrixGraph::transformToListOfEdges() {
    auto temp = new ListOfEdgesGraph(n, r, w);
    for (int i = 0; i < n; ++i) {
        for (int j = ((r == 1) ? 0 : i); j < n; ++j) {
            if (data[i][j] != 0) {
                temp->addEdge(i + 1, j + 1, data[i][j]);
            }
        }
    }
    return temp;
}

std::vector<std::vector<int>>& AdjMatrixGraph::getAdjMatrix() {
    return data;
}

IGraph* AdjMatrixGraph::getCopy() {
    return new AdjMatrixGraph(*this);
}

void AdjMatrixGraph::getProperties(int& n, int& r, int& w) {
    n = this->n;
    r = this->r;
    w = this->w;
}

int AdjMatrixGraph::getSize() {
    return n;
}


////////////////////////////////////////////////////////////////////////////////////////////////////
// AdjList representation.
////////////////////////////////////////////////////////////////////////////////////////////////////

void AdjListGraph::readGraph(std::istream& stream) {
    stream >> n;
    stream >> r >> w;
    std::vector<std::map<int, int>>(n).swap(data);
    std::string line;
    std::getline(stream, line);
    for (int i = 0; i < n; ++i) {
        std::getline(stream, line);
        std::istringstream lineStream(line);
        int bi = 0;
        int wi = 0;
        while (lineStream >> bi) {
            if (w == 1) {
                lineStream >> wi;
            }
            data[i].emplace(bi, wi);
        }
    }
}

void AdjListGraph::writeGraph(std::ostream& stream) {
    stream << "L" << " " << n << "\n";
    stream <<  r  << " " << w << "\n";
    for (int i = 0; i < n; ++i) {
        std::ostringstream lineStream;
        for (auto& item : data[i]) {
            lineStream << item.first << " ";
            if (w == 1) {
                lineStream << item.second << " ";
            }
        }
        auto line = lineStream.str();
        if (!line.empty()) {
            line.erase(line.end() - 1);
        }
        stream << line << "\n";
    }
}

void AdjListGraph::addEdge(int from, int to, int weight) {
    data[from - 1][to] = weight;
    if (r == 0) {
        data[to - 1][from] = weight;
    }
}

void AdjListGraph::removeEdge(int from, int to) {
    data[from - 1].erase(to);
    if (r == 0) {
        data[to - 1].erase(from);
    }
}

int AdjListGraph::changeEdge(int from, int to, int weight) {
    auto temp = data[from - 1][to];
    data[from - 1][to] = weight;
    if (r == 0) {
        data[to - 1][from] = weight;
    }
    return temp;
}

int AdjListGraph::getEdge(int from, int to) {
    auto item = data[from - 1].find(to);
    return (item != data[from - 1].end()) ? item->second : -1;
}

IGraph* AdjListGraph::transformToAdjMatrix() {
    auto temp = new AdjMatrixGraph(n, r, w);
    for (int i = 0; i < n; ++i) {
        for (auto& item : data[i]) {
            if ((r == 0) && (i + 1 > item.first)) {
                continue;
            }
            temp->addEdge(i + 1, item.first, item.second);
        }
    }
    return temp;
}

IGraph* AdjListGraph::transformToAdjList() {
    return this;
}

IGraph* AdjListGraph::transformToListOfEdges() {
    auto temp = new ListOfEdgesGraph(n, r, w);
    for (int i = 0; i < n; ++i) {
        for (auto& item : data[i]) {
            if ((r == 0) && (i + 1 > item.first)) {
                continue;
            }
            temp->addEdge(i + 1, item.first, item.second);
        }
    }
    return temp;
}

std::vector<std::map<int, int>>& AdjListGraph::getAdjList() {
    return data;
}

IGraph* AdjListGraph::getCopy() {
    return new AdjListGraph(*this);
}

void AdjListGraph::getProperties(int& n, int& r, int& w) {
    n = this->n;
    r = this->r;
    w = this->w;
}

int AdjListGraph::getSize() {
    return n;
}


////////////////////////////////////////////////////////////////////////////////////////////////////
// ListOfEdges representation.
////////////////////////////////////////////////////////////////////////////////////////////////////

void ListOfEdgesGraph::readGraph(std::istream& stream) {
    stream >> n >> m;
    stream >> r >> w;
    for (int i = 0; i < m; ++i) {
        int ai = 0;
        int bi = 0;
        int wi = 0;
        stream >> ai >> bi;
        if (w == 1) {
            stream >> wi;
        }
        if ((r == 0) && (ai > bi)) {
            std::swap(ai, bi);
        }
        data.emplace_back(ai, bi, wi);
    }
}

void ListOfEdgesGraph::writeGraph(std::ostream& stream) {
    stream << "E" << " " << n << " " << m << "\n";
    stream <<  r  << " " << w << "\n";
    for (int i = 0; i < m; ++i) {
        int ai;
        int bi;
        int wi;
        std::tie(ai, bi, wi) = data[i];
        stream << ai << " " << bi;
        if (w == 1) {
            stream << " " << wi;
        }
        stream << "\n";
    }
}

void ListOfEdgesGraph::addEdge(int from, int to, int weight) {
    if ((r == 0) && (from > to)) {
        std::swap(from, to);
    }
    data.emplace_back(from, to, weight);
    ++m;
}

void ListOfEdgesGraph::removeEdge(int from, int to) {
    if ((r == 0) && (from > to)) {
        std::swap(from, to);
    }
    auto edge = std::find_if(data.begin(), data.end(), [from, to](std::tuple<int, int, int>& edge) {
        return (std::get<0>(edge) == from) && (std::get<1>(edge) == to);
    });
    data.erase(edge);
    --m;
}

int ListOfEdgesGraph::changeEdge(int from, int to, int weight) {
    if ((r == 0) && (from > to)) {
        std::swap(from, to);
    }
    auto edge = std::find_if(data.begin(), data.end(), [from, to](std::tuple<int, int, int>& edge) {
        return (std::get<0>(edge) == from) && (std::get<1>(edge) == to);
    });
    std::swap(std::get<2>(*edge), weight);
    return weight;
}

int ListOfEdgesGraph::getEdge(int from, int to) {
    if ((r == 0) && (from > to)) {
        std::swap(from, to);
    }
    auto edge = std::find_if(data.begin(), data.end(), [from, to](std::tuple<int, int, int>& edge) {
        return (std::get<0>(edge) == from) && (std::get<1>(edge) == to);
    });
    return (edge != data.end()) ? std::get<2>(*edge) : -1;
}

IGraph* ListOfEdgesGraph::transformToAdjMatrix() {
    auto temp = new AdjMatrixGraph(n, r, w);
    for (auto& edge : data) {
        int ai;
        int bi;
        int wi;
        std::tie(ai, bi, wi) = edge;
        temp->addEdge(ai, bi, wi);
    }
    return temp;
}

IGraph* ListOfEdgesGraph::transformToAdjList() {
    auto temp = new AdjListGraph(n, r, w);
    for (auto& edge : data) {
        int ai;
        int bi;
        int wi;
        std::tie(ai, bi, wi) = edge;
        temp->addEdge(ai, bi, wi);
    }
    return temp;
}

IGraph* ListOfEdgesGraph::transformToListOfEdges() {
    return this;
}

std::vector<std::tuple<int, int, int>>& ListOfEdgesGraph::getListOfEdges() {
    return data;
}

IGraph* ListOfEdgesGraph::getCopy() {
    return new ListOfEdgesGraph(*this);
}

void ListOfEdgesGraph::getProperties(int& n, int& r, int& w) {
    n = this->n;
    r = this->r;
    w = this->w;
}

int ListOfEdgesGraph::getSize() {
    return n;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
// Graph class.
////////////////////////////////////////////////////////////////////////////////////////////////////

class Graph {
private:
    struct QueueComparator {
        bool operator()(const std::pair<int, int>& a, const std::pair<int, int>& b) {
            int av;
            int aw;
            int bv;
            int bw;
            std::tie(av, aw) = a;
            std::tie(bv, bw) = b;
            if ((aw == -1) && (bw != -1)) {
                return true;
            }
            if ((aw != -1) && (bw == -1)) {
                return false;
            }

            if (aw > bw) {
                return true;
            }
            if (aw == bw) {
                return av > bv;
            }
            return false;
        }
    };

    struct Edge {
        int from;
        int to;
        int next;
        int capacity;
        int flow;
    };

private:
    IGraph* graph;
    char    type;

public:
    Graph(char type, int n, int r, int w)
        : type(type)
    {
        switch (type) {
        case 'C':
            graph = new AdjMatrixGraph(n, r, w);
            break;
        case 'L':
            graph = new AdjListGraph(n, r, w);
            break;
        case 'E':
            graph = new ListOfEdgesGraph(n, r, w);
            break;
        }
    }

    Graph(int n)
        : type('L')
    {
        graph = new AdjListGraph(n, 0, 1);
    }

    Graph(const Graph& other)
        : graph(other.graph->getCopy())
        , type(other.type)
    {
    }

    Graph()
        : graph(nullptr)
        , type('0')
    {
    }

    ~Graph() {
        if (graph != nullptr) {
            delete graph;
        }
    }

    void readGraph(std::string fileName) {
        std::ifstream file(fileName);
        type = file.get();
        switch (type) {
        case 'C':
            graph = new AdjMatrixGraph();
            break;
        case 'L':
            graph = new AdjListGraph();
            break;
        case 'E':
            graph = new ListOfEdgesGraph();
            break;
        }
        graph->readGraph(file);
    }

    void writeGraph(std::string fileName) {
        std::ofstream file(fileName);
        graph->writeGraph(file);
    }

    void addEdge(int from, int to, int weight) {
        graph->addEdge(from, to, weight);
    }

    void removeEdge(int from, int to) {
        graph->removeEdge(from, to);
    }

    int changeEdge(int from, int to, int weight) {
        return graph->changeEdge(from, to, weight);
    }

    int getEdge(int from, int to) {
        return graph->getEdge(from, to);
    }

    void transformToAdjMatrix() {
        auto temp = graph->transformToAdjMatrix();
        if (temp != graph) {
            delete graph;
            graph = temp;
        }
        type = 'C';
    }

    void transformToAdjList() {
        auto temp = graph->transformToAdjList();
        if (temp != graph) {
            delete graph;
            graph = temp;
        }
        type = 'L';
    }

    void transformToListOfEdges() {
        auto temp = graph->transformToListOfEdges();
        if (temp != graph) {
            delete graph;
            graph = temp;
        }
        type = 'E';
    }

    Graph getSpanningTreeKruscal() {
        transformToListOfEdges();
        auto& edges = graph->getListOfEdges();
        std::sort(edges.begin(), edges.end(), ListOfEdgesGraph::EdgeComparator());
        int n;
        int r;
        int w;
        graph->getProperties(n, r, w);
        DSU dsu(n);
        Graph result('E', n, r, w);
        int a;
        int b;
        int c;
        for (auto& edge : edges) {
            std::tie(a, b, c) = edge;
            if (dsu.find(a) != dsu.find(b)) {
                dsu.join(a, b);
                result.addEdge(a, b, c);
            }
        }
        return result;
    }

    Graph getSpanningTreePrima() {
        transformToAdjList();
        int n;
        int r;
        int w;
        graph->getProperties(n, r, w);
        Graph result('E', n, r, w);

        std::vector<bool> used(n, false);
        std::vector<int> key(n, -1);
        std::vector<int> parent(n, -1);
        std::priority_queue<std::pair<int, int>, std::vector<std::pair<int, int>>, QueueComparator> queue;

        key[0] = 0;
        for (int i = 0; i < n; ++i) {
            queue.emplace(i, key[i]);
        }

        while (!queue.empty()) {
            int min = queue.top().first;
            key[min] = queue.top().second;
            queue.pop();

            if (used[min]) {
                continue;
            }
            used[min] = true;

            if (parent[min] != -1) {
                result.addEdge(min + 1, parent[min] + 1, graph->getEdge(min + 1, parent[min] + 1));
            }

            for (auto& edge : graph->getAdjList()[min]) {
                int v = edge.first - 1;
                int w = edge.second;
                if ((!used[v]) && ((w < key[v]) || (key[v] == -1))) {
                    key[v] = w;
                    parent[v] = min;
                    queue.emplace(v, key[v]);
                }
            }
        }

        return result;
    }

    Graph getSpanningTreeBoruvka() {
        transformToListOfEdges();
        int n;
        int r;
        int w;
        graph->getProperties(n, r, w);
        DSU dsu(n);
        Graph result('E', n, r, w);

        auto& edges = graph->getListOfEdges();
        int m = edges.size();
        int k = n;

        while (k > 1) {
            std::vector<int> min(n, -1);
            bool isForest = true;
            for (int i = 0; i < m; ++i) {
                int from = dsu.find(std::get<0>(edges[i])) - 1;
                int to   = dsu.find(std::get<1>(edges[i])) - 1;
                int weight = std::get<2>(edges[i]);
                if (from != to) {
                    if ((min[from] == -1) || (weight < std::get<2>(edges[min[from]]))) {
                        min[from] = i;
                        isForest = false;
                    }
                    if ((min[to] == -1) || (weight < std::get<2>(edges[min[to]]))) {
                        min[to] = i;
                        isForest = false;
                    }
                }
            }

            if (isForest) {
                break;
            }

            for (int i = 0; i < n; ++i) {
                if (min[i] != -1) {
                    int from;
                    int to;
                    int weight;
                    std::tie(from, to, weight) = edges[min[i]];
                    if (dsu.find(from) != dsu.find(to)) {
                        result.addEdge(from, to, weight);
                        dsu.join(from, to);
                        --k;
                    }
                }
            }
        }

        return result;
    }

    int checkEuler(bool& circleExist) {
        circleExist = false;
        int oddDegree = 0;
        int startVert = 0;

        transformToAdjList();
        auto& adjList = graph->getAdjList();

        int n = graph->getSize();
        std::vector<int> degrees(n, 0);
        std::vector<bool> distinct(n, false);

        for (int i = 0; i < int(adjList.size()); ++i) {
            degrees[i] = adjList[i].size();
            if ((degrees[i] % 2) != 0) {
                ++oddDegree;
                startVert = i + 1;
            }
        }

        if (oddDegree > 2) {
            return 0;
        }

        transformToListOfEdges();
        DSU dsu(n);
        for (auto& edge : graph->getListOfEdges()) {
            int a;
            int b;
            int c;
            std::tie(a, b, c) = edge;
            dsu.join(a, b);
        }

        for (int i = 1; i < n; ++i) {
            int a = dsu.find(i) - 1;
            int b = dsu.find(i + 1) - 1;
            if (a != b) {
                distinct[a] = true;
                distinct[b] = true;
            }
        }

        int count = 0;
        for (int i = 0; i < n; ++i) {
            if (distinct[i] && (degrees[i] > 0)) {
                ++count;
            }
            if (count > 1) {
                return 0;
            }
        }

        if (startVert == 0) {
            circleExist = true;
            startVert = 1;
        }
        return startVert;
    }

    bool isBridge(int from, int to) {
        transformToAdjList();
        int n = graph->getSize();
        std::queue<int> queue;
        std::vector<bool> used(n + 1, false);
        queue.push(from);
        used[from] = true;
        while (!queue.empty()) {
            int v = queue.front();
            queue.pop();
            for (auto& edge : graph->getAdjList()[v - 1]) {
                int next = edge.first;
                if ((v == from) && (next == to)) {
                    continue;
                }
                if (!used[next]) {
                    if (next == to) {
                        return false;
                    }
                    queue.push(next);
                    used[next] = true;
                }
            }
        }
        return true;
    }

    std::vector<int> getEuleranTourFleri() {
        std::vector<int> result;
        bool circleExist;
        int start = checkEuler(circleExist);
        if (start == 0) {
            return result;
        }

        transformToAdjList();
        Graph g(*this);
        auto& adjList = g.graph->getAdjList();

        int currV = start;
        result.push_back(currV);
        for (;;) {
            int deg = adjList[currV - 1].size();
            if (deg == 0) {
                break;
            }
            bool flag = true;
            for (auto& edge : adjList[currV - 1]) {
                int nextV = edge.first;
                if ((deg == 1) || !g.isBridge(currV, nextV)) {
                    flag = false;
                    g.removeEdge(currV, nextV);
                    currV = nextV;
                    result.push_back(currV);
                    break;
                }
            }
            if (flag) {
                int nextV = adjList[currV - 1].begin()->first;
                g.removeEdge(currV, nextV);
                currV = nextV;
                result.push_back(currV);
            }
        }
        return result;
    }

    std::vector<int> getEuleranTourEffective() {
        std::vector<int> result;
        bool circleExist;
        int start = checkEuler(circleExist);
        if (start == 0) {
            return result;
        }

        transformToAdjList();
        Graph g(*this);
        auto& adjList = g.graph->getAdjList();

        std::stack<int> stack;
        stack.push(start);
        while (!stack.empty()) {
            int currV = stack.top();
            for (auto& edge : adjList[currV - 1]) {
                int nextV = edge.first;
                stack.push(nextV);
                g.removeEdge(currV, nextV);
                break;
            }
            if (currV == stack.top()) {
                stack.pop();
                result.push_back(currV);
            }
        }
        return result;
    }

    bool checkBipart(std::vector<char>& marks) {
        transformToAdjList();
        std::queue<int> queue;
        int n = graph->getSize();
        
        for (int i = 1; i <= n; ++i) {
            if ((marks[i] != 'A') && (marks[i] != 'B')) {
                marks[i] = 'A';
                queue.push(i);
                while (!queue.empty()) {
                    int currV = queue.front();
                    queue.pop();
                    for (auto& edge : graph->getAdjList()[currV - 1]) {
                        int nextV = edge.first;
                        if ((marks[nextV] != 'A') && (marks[nextV] != 'B')) {
                            marks[nextV] = ((marks[currV] == 'A') ? 'B' : 'A');
                            queue.push(nextV);
                        }
                        else {
                            if (marks[nextV] == marks[currV]) {
                                return false;
                            }
                        }
                    }
                }
            }
        }
        return true;
    }

    bool kuhnDFS(int currV, std::vector<bool>& used, std::vector<int>& currBipart) {
        if (used[currV]) {
            return false;
        }
        used[currV] = true;
        for (auto& edge : graph->getAdjList()[currV - 1]) {
            int nextV = edge.first;
            if ((currBipart[nextV] == -1) || kuhnDFS(currBipart[nextV], used, currBipart)) {
                currBipart[nextV] = currV;
                return true;
            }
        }
        return false;
    }

    std::vector<std::pair<int, int>> getMaximumMatchingBipart() {
        transformToAdjList();
        int n = graph->getSize();
        std::vector<std::pair<int, int>> result;
        std::vector<int> currBipart(n + 1, -1);
        std::vector<char> marks(n + 1, 'Z');
        std::vector<bool> usedA(n + 1, false);
        std::vector<bool> usedB(n + 1, false);

        for (int i = 1; i <= n; ++i) {
            if (marks[i] == 'B') {
                continue;
            }
            for (auto& edge : graph->getAdjList()[i - 1]) {
                if (currBipart[edge.first] == -1) {
                    currBipart[edge.first] = i;
                    usedB[i] = true;
                    break;
                }
            }
        }

        if (checkBipart(marks)) {
            for (int i = 1; i <= n; ++i) {
                if (usedB[i] || (marks[i] == 'B')) {
                    continue;
                }
                usedA.assign(n + 1, false);
                kuhnDFS(i, usedA, currBipart);
            }
            for (int i = 1; i <= n; ++i) {
                if ((marks[i] == 'B') && (currBipart[i] != -1)) {
                    result.emplace_back(currBipart[i], i);
                }
            }
        }
        return result;
    }

    int ffDFS(int source, int sink, int flow, std::vector<Edge>& edges, std::vector<int>& head, std::vector<bool>& used) {
        if (source == sink) {
            return flow;
        }
        used[source] = true;
        for (int i = head[source]; i != -1; i = edges[i].next) {
            if (!used[edges[i].to] && (edges[i].flow < edges[i].capacity)) {
                int pushed = ffDFS(edges[i].to, sink, std::min(flow, edges[i].capacity - edges[i].flow), edges, head, used);
                if (pushed) {
                    edges[i].flow += pushed;
                    edges[i ^ 1].flow -= pushed;
                    return pushed;
                }
            }
        }
        return 0;
    }

    Graph flowFordFulkerson(int sourse, int sink) {
        int n;
        int r;
        int w;
        graph->getProperties(n, r, w);
        Graph result(type, n, r, w);

        std::vector<Edge> edges;
        std::vector<int> head(n + 1, -1);
        std::vector<bool> used(n + 1, false);

        transformToListOfEdges();
        for (auto& edge : graph->getListOfEdges()) {
            int a;
            int b;
            int w;
            std::tie(a, b, w) = edge;
            edges.push_back(Edge{a, b, head[a], w, 0});
            edges.push_back(Edge{b, a, head[b], 0, 0});
            head[a] = edges.size() - 2;
            head[b] = edges.size() - 1;
        }

        for (;;) {
            if (ffDFS(sourse, sink, std::numeric_limits<int>::max(), edges, head, used) == 0) {
                break;
            }
            used.assign(n + 1, false);
        }

        for (int i = 0; i < int(edges.size()); i += 2) {
            result.addEdge(edges[i].from, edges[i].to, edges[i].flow);
        }
        return result;
    }

    int dinitzDFS(int source, int sink, int flow, std::vector<int>& vertexLayer,
        std::map<std::pair<int, int>, int>& edgeCapacity, std::map<std::pair<int, int>, int>& edgeFlow) {
        if ((source == sink) || (flow == 0)) {
            return flow;
        }
        auto& adjList = graph->getAdjList();
        for (auto& edge : adjList[source - 1]) {
            int to = edge.first;
            if (vertexLayer[to] != vertexLayer[source] + 1) {
                continue;
            }
            auto tmpEdge = std::make_pair(source, to);
            auto revEdge = std::make_pair(to, source);
            int newFlow = std::min(flow, edgeCapacity[tmpEdge] - edgeFlow[tmpEdge]);
            int pushed = dinitzDFS(to, sink, newFlow, vertexLayer, edgeCapacity, edgeFlow);
            if (pushed) {
                adjList[source - 1][to] -= pushed;
                adjList[to - 1][source] += pushed;
                edgeFlow[tmpEdge] += pushed;
                edgeFlow[revEdge] -= pushed;
                return pushed;
            }
        }
        return 0;
    }

    Graph flowDinitz(int sourse, int sink) {
        std::map<std::pair<int, int>, int> edgeCapacity;
        std::map<std::pair<int, int>, int> edgeFlow;
        int n;
        int r;
        int w;
        graph->getProperties(n, r, w);
        Graph g('L', n, r, w);
        Graph result(type, n, r, w);

        transformToAdjList();
        for (int i = 0; i < n; ++i) {
            for (auto& edge : graph->getAdjList()[i]) {
                int to = edge.first;
                int weight = edge.second;
                g.addEdge(to, i + 1, 0);
                g.addEdge(i + 1, to, weight);
                auto tmpEdge = std::make_pair(i + 1, to);
                auto revEdge = std::make_pair(to, i + 1);
                edgeCapacity[tmpEdge] = weight;
                edgeCapacity[revEdge] = weight;
                edgeFlow[tmpEdge] = 0;
                edgeFlow[revEdge] = weight;
            }
        }

        for (;;) {
            std::vector<int> vertexLayer(n + 1, -1);
            std::queue<int> queue;
            vertexLayer[sourse] = 0;
            queue.push(sourse);
            while (!queue.empty() && (vertexLayer[sink] == -1)) {
                int currV = queue.front();
                queue.pop();
                for (auto& edge : g.graph->getAdjList()[currV - 1]) {
                    int to = edge.first;
                    auto tmpEdge = std::make_pair(currV, to);
                    if ((vertexLayer[to] == -1) && (edgeFlow[tmpEdge] < edgeCapacity[tmpEdge])) {
                        queue.push(to);
                        vertexLayer[to] = vertexLayer[currV] + 1;
                    }
                }
            }
            if (g.dinitzDFS(sourse, sink, std::numeric_limits<int>::max(), vertexLayer, edgeCapacity, edgeFlow) == 0) {
                break;
            }
        }

        for (int i = 0; i < n; ++i) {
            for (auto& edge : g.graph->getAdjList()[i]) {
                int to = edge.first;
                if (i >= to) {
                    result.addEdge(to, i + 1, edge.second);
                }
            }
        }
        return result;
    }
};

#endif//GRAPH_H
